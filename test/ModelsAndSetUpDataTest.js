"use strict";
var chai = require('chai');
var expect = chai.expect;
var sinon = require('sinon');
var SetUpData = require('../setUpData').default;
var Models = require('../model/modelsBuild');
var mongoose = require('mongoose');

mongoose.models = {};
mongoose.modelSchemas = {};

var models =  new Models.default();


describe('Setup Data Testing', function() {

    it('Set Up Data Creates Admin User', function() {

        SetUpData.execute(models);
        models.UserModel.find({email:'wetalkcode@gmail.com'}, function(err, users) {
            expect(users.length).to.equal(1);
            expect(users[0].email).to.equal('wetalkcode@gmail.com');
            expect(users[0].commentProduct).to.be.true;
            expect(users[0].commentProductWeb).to.equal('wetalkcode.co.uk');
            expect(users[0].commentProductVersion).to.equal('PRO');
            expect(users[0].commentProductPayType).to.equal('YEARLY');
        });
    });

    it('Set Up Data Creates Articles', function() {

        SetUpData.execute(models);
        models.BlogModel.find({}, function(err, blogs) {
            expect(blogs.length).to.equal(4);
        });
    });
});
