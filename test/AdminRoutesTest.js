"use strict";
var chai = require('chai');
var expect = chai.expect;
var sinon = require('sinon');
var express = require('express');
var AdminRoutes = require('../routes/adminRoutes').default;
var Models = require('../model/modelsBuild');
var mongoose = require('mongoose');
var moment = require('moment');

mongoose.models = {};
mongoose.modelSchemas = {};

var models =  new Models.default();


describe('Admin routes Testing', function() {
  it('Admin routes are added to express app', function() {
    var app = express();
    var adminroutes = new AdminRoutes(models);
    adminroutes.configure(app);
    var routes = [];
    app._router.stack.map(function(layer){
      if(layer.route !== undefined){
        if(layer.route.methods.get === true)
          routes[layer.route.path+"_get"] = layer;
        else if (layer.route.methods.post === true)
          routes[layer.route.path+"_post"] = layer;
      }
    });

    expect(routes['/admin/blog/edit/:id_get']).to.not.be.undefined;
    expect(routes['/admin/blog/delete/:id_get']).to.not.be.undefined;
    expect(routes['/admin/blog_post']).to.not.be.undefined;
    expect(routes['/admin/blogs_get']).to.not.be.undefined;
  });

    it('Admin show blogs', function() {
      var app = express();
      var adminroutes = new AdminRoutes(models);


      var request = {};

      var response = {
        render: function(view, callback){
          console.log("EP");
        }
      };

      var responseSpy = sinon.spy(response, 'render');

      //adminroutes.showBlogs(request, response);

      //expect(responseSpy.calledOnce).to.be.true;
    });
});
