"use strict";
var chai = require('chai');
var expect = chai.expect;
var sinon = require('sinon');
var express = require('express');
var Settings = require('../configuration/settings').default;

var login = require('./utils/utils.js');
var Models = require('../model/modelsBuild');

var request = require('supertest');
var server = require('../app').default;
var models = new Models.default();
var EncryptService = require('../services/encryptService').default;
var EmailService = require('../services/emailService').default;

describe('Exception routes Testing', function() {

    after(function(done) {
        server.close();
        done();
    });

    before(function(done) {
        Settings.setEnvToDevelopment();
        models.CommentModel.find(function(err, comments) {
            for (var comment of comments) {
                comment.remove();
            }
            done();
        });
    });


    it('App Exception handles 404', function(done) {
        request(server)
            .get('/notfound')
            .expect(404, done);
    });

    it('App handles /about', function(done) {
        request(server)
            .get('/about')
            .expect(function(res) {
                if (res.text.indexOf("Joaquim Orra owner of WeTalkCode") < 0) {
                    throw new Error("Not title in about page");
                }
                if (res.status != 200) {
                    throw new Error("No status code 200");
                }
            })
            .end(done);
    });

    it('App handles /', function(done) {
        request(server)
            .get('/')
            .expect(function(res) {
                if (res.text.indexOf("title>WeTalkCode") < 0) {
                    throw new Error("Not title in main page");
                }
                if (res.status != 200) {
                    throw new Error("No status code 200");
                }
            })
            .end(done);
    });

    it('App handles /admin/blogs when not logged redirects to login', function(done) {
        request(server)
            .get('/admin/blogs')
            .expect(function(res) {
                if (res.text.indexOf("Found. Redirecting to /login") < 0) {
                    throw new Error("Not title in main page");
                }
                if (res.status != 302) {
                    throw new Error("No status code 200");
                }
            })
            .end(done);
    });

    it('App handles /admin/blogs when logged', function(done) {
        var requestApp = request(server);
        login.login(requestApp, function(loginAgent) {
            var requestedAdmin = requestApp.get('/admin/blogs')
            loginAgent.attachCookies(requestedAdmin);
            requestedAdmin.expect(200, done);
        });
    });

    it('App handles /admin/blog when logged without form data', function(done) {
        var requestApp = request(server);
        login.login(requestApp, function(loginAgent) {
            var requestedAdmin = requestApp.post('/admin/blog')
            loginAgent.attachCookies(requestedAdmin);
            requestedAdmin.expect(500, done);
        });
    });

    it('App handles /admin/blog when logged with form data for new Blog', function(done) {
        var requestApp = request(server);
        var agent;
        login.login(requestApp, function(loginAgent) {
            agent = loginAgent;
            var requestedAdminCreateBlog = requestApp.post('/admin/blog')
                .type('form')
                .set('Accept', 'application/json')
                .send({
                    "title": "title",
                    "body": "body",
                    "tags": "tags",
                    "path": "path",
                    "published": "2016-06-06"
                })
            loginAgent.attachCookies(requestedAdminCreateBlog);
            requestedAdminCreateBlog
                .expect(200, done);
        });
    });

    it('App handles edit blog', function(done) {
        var requestApp = request(server);
        var agent;
        login.login(requestApp, function(loginAgent) {
            agent = loginAgent;
            models.BlogModel.find({
                title: 'title'
            }, function(err, blogs) {
                expect(blogs.length).to.be.equal(1);
                var blog = blogs[0];
                var requestedAdminEditBlog = requestApp.get('/admin/blog/edit/' + blog._id)
                loginAgent.attachCookies(requestedAdminEditBlog);
                requestedAdminEditBlog
                    .expect(function(res) {
                        if (res.text.indexOf("Admin blog panel, WeTalkCode") < 0) {
                            throw new Error("Not title in main page");
                        }
                    })
                    .expect(200, done);
            });
        });
    });

    it('App handles delete blog', function(done) {
        var requestApp = request(server);
        var agent;
        login.login(requestApp, function(loginAgent) {
            agent = loginAgent;
            models.BlogModel.find({
                title: 'title'
            }, function(err, blogs) {
                expect(blogs.length).to.be.equal(1);
                var blog = blogs[0];
                var requestedAdminDeleteBlog = requestApp.get('/admin/blog/delete/' + blog._id)
                loginAgent.attachCookies(requestedAdminDeleteBlog);
                requestedAdminDeleteBlog
                    .expect(function(res) {
                        if (res.body.status != 'success')
                            throw new Error("Not success when deleting");
                    }).end(done);
            });
        });
    });

    it('App handles  /admin/blog get', function(done) {
        var requestApp = request(server);
        var agent;
        login.login(requestApp, function(loginAgent) {
            agent = loginAgent;
            var requestedAdminCreateBlog = requestApp.get('/admin/blog');
            loginAgent.attachCookies(requestedAdminCreateBlog);
            requestedAdminCreateBlog
                .expect(200, done);
        });
    });

    it('App handles  /blog/view/ get no existing blog', function(done) {
        request(server)
            .get('/blog/view/wewewe')
            .expect(500)
            .end(done);
    });

    it('App handles  /blog/view/ get existing blog', function(done) {
        request(server)
            .get('/blog/view/nightwatch')
            .expect(200)
            .end(done);
    });

    it('App handles  creating comment', function(done) {
        request(server)
            .post('/blog/comment/create')
            .type('form')
            .set('Accept', 'application/json')
            .send({
                "name": "wetalkcode@gmail.com",
                "body": "My message 'hole' | {}",
                "path": "nightwatch"
            })
            .expect(200)
            .expect(function(res) {
                if (res.body.status != 'success')
                    throw new Error("Not success when creating comment");
            }).end(done);
    });

    it('App handles getting comment', function(done) {
        request(server)
            .get('/blog/comments/nightwatch')
            .expect(200)
            .expect(function(res) {
                var comment = res.body[0];
                if (comment.name != 'wetalkcode@gmail.com' || comment.body != "My message 'hole' | {}" || comment.path != 'nightwatch')
                    throw new Error("Not success when creating comment");
            }).end(done);
    });

    it('App /product/commentswtc, main page for product', function(done) {
        request(server)
            .get('/product/commentswtc')
            .expect(200)
            .expect(function(res) {
                if (res.text.indexOf('title>WeTalkCode') < 0)
                    throw new Error("Not rendering comments Product main page");
            }).end(done);
    });

    it('App /product/commentswtc/get/:type , is available when logged', function(done) {
        request(server)
            .get('/product/commentswtc/get/free')
            .expect(function(res) {
                if (res.text.indexOf("Found. Redirecting to /login") < 0) {
                    throw new Error("Not title in main page");
                }
                if (res.status != 302) {
                    throw new Error("No status code 200");
                }
            })
            .end(done);
    });

    it('App /product/commentswtc/get/:type , when type is free', function(done) {

        var requestApp = request(server);
        var agent;
        login.login(requestApp, function(loginAgent) {
            agent = loginAgent;
            var requestedProductType = requestApp.get('/product/commentswtc/get/free');
            loginAgent.attachCookies(requestedProductType);
            requestedProductType
                .expect(200, done);
        });
    });

    it('App /product/commentswtc/get/:type , when type is pro', function(done) {

        var requestApp = request(server);
        var agent;
        login.login(requestApp, function(loginAgent) {
            agent = loginAgent;
            var requestedProductType = requestApp.get('/product/commentswtc/get/pro');
            loginAgent.attachCookies(requestedProductType);
            requestedProductType
                .expect(200, done);
        });
    });

    it('App /product/commentswtc/get/:type , when type is not reconized', function(done) {

        var requestApp = request(server);
        var agent;
        login.login(requestApp, function(loginAgent) {
            agent = loginAgent;
            var requestedFreeProductType = requestApp.get('/product/commentswtc/get/profree');
            loginAgent.attachCookies(requestedFreeProductType);
            requestedFreeProductType
                .expect(200)
                .expect(function(res) {
                    expect(res.text.indexOf('User could not be verified! try again') > 0).to.be.true;
                })
                .end(done);
        });
    });


    it('App /product/generateKey/:theweb', function(done) {

        var requestApp = request(server);
        var agent;
        login.login(requestApp, function(loginAgent) {
            agent = loginAgent;
            var requestedKeyForProduct = requestApp.post('/product/generateKey/wetalkcode.co.uk');
            loginAgent.attachCookies(requestedKeyForProduct);
            requestedKeyForProduct
                .expect(200)
                .expect(function(res) {
                    if (res.body.status != 'success' || res.body.key === undefined)
                        throw new Error("Not success when generating product key");
                    if (!EncryptService.isValid('wetalkcode@gmail.comwetalkcode.co.uk', res.body.key)) {
                        throw new Error("The product key is not valid");
                    }
                })
                .end(done);
        });
    });

    it('App /client/dashboard', function(done) {

        var requestApp = request(server);
        var agent;
        login.login(requestApp, function(loginAgent) {
            agent = loginAgent;
            var requestClientDashboard = requestApp.get('/client/dashboard');
            loginAgent.attachCookies(requestClientDashboard);
            requestClientDashboard
                .expect(200)
                .expect(function(res) {
                    expect(res.text.indexOf('Welcome Cristy i Kim') > 0).to.be.true;
                })
                .end(done);
        });
    });

    it('App /register with some data missing show Error page', function(done) {

      request(server)
          .post('/register')
          .type('form')
          .set('Accept', 'application/json')
          .send({username : "", company : "Company" , email:"email@com.com", password:"password"})
          .expect(200)
          .expect(function(res) {
              expect(res.text.indexOf('Registration form not complete!') > 0).to.be.true;
          })
          .end(done);
    });

    it('App /register with existing user', function(done) {

      request(server)
          .post('/register')
          .type('form')
          .set('Accept', 'application/json')
          .send({username : "Username", company : "Company" , email:"wetalkcode@gmail.com", password:"password"})
          .expect(200)
          .expect(function(res) {
              expect(res.text.indexOf('User already exist!') > 0).to.be.true;
          })
          .end(done);
    });

    it('App /register with none existing user', function(done) {

      request(server)
          .post('/register')
          .type('form')
          .set('Accept', 'application/json')
          .send({username : "Username", company : "Company" , email:"wetalkcode@gmail.com" + Math.random() , password:"password"})
          .expect(200)
          .expect(function(res) {
              expect(res.text.indexOf('One last step is email verification') > 0).to.be.true;
          })
          .end(done);
    });



});
