"use strict";
var chai = require('chai');
var expect = chai.expect;
var sinon = require('sinon');
var ConfigApp = require('../configuration/config').default;


describe('Configuration App Testing', function() {
  it('App configuration adds moment to local, login routes, setemap route and robots route', function() {

    var app = ConfigApp.configure({});
    expect(app).to.not.be.null;
    expect(app).to.not.be.undefined;
    expect(app.locals.moment).to.be.an.object;
    expect(app.settings.views).to.be.equal("assets");
    expect(app.settings['view engine']).to.be.equal("jade");

    var routes = [];
    app._router.stack.map(function(layer){
      if(layer.route !== undefined){
        if(layer.route.methods.get === true)
          routes[layer.route.path+"_get"] = layer;
        else if (layer.route.methods.post === true)
          routes[layer.route.path+"_post"] = layer;
      }
    });

    expect(routes['/login_get']).to.not.be.undefined;
    expect(routes['/login_post']).to.not.be.undefined;
    expect(routes['/logout_get']).to.not.be.undefined;
    expect(routes['/sitemap.xml_get']).to.not.be.undefined;
    expect(routes['/robots.txt_get']).to.not.be.undefined;

  });
});
