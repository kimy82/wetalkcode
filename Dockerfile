FROM node:argon

#MONGODB
RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927
RUN echo "deb http://repo.mongodb.org/apt/debian wheezy/mongodb-org/3.2 main" tee /etc/apt/sources.list.d/mongodb-org-3.2.list
RUN apt-get update
RUN apt-get install -y mongodb
RUN mkdir -p /data/db

#SUPERVISOR
RUN apt-get install -y supervisor
COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf

#WORKDIRECTORY
RUN mkdir -p /usr/src/wtc
WORKDIR /usr/src/wtc

#DEPENDENCIES
COPY package.json /usr/src/wtc/
RUN npm install

#SOURCECODE
COPY . /usr/src/wtc

EXPOSE 7070
CMD ["/usr/bin/supervisord"]
#CMD [ "npm", "start" ]
