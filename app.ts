import SetupData from './setUpData';
import Models from './model/modelsBuild';
import AdminRoutes from './routes/adminRoutes';
import BlogRoutes from './routes/blogRoutes';
import ProductRoutes from './routes/productRoutes';
import ClientRoutes from './routes/clientRoutes';
import RegistrationRoutes from './routes/registrationRoutes';
import ExceptionRoutes from './routes/exceptionRoutes';
import ConfigApp from './configuration/config';
import * as express from 'express';
import Settings from './configuration/settings';
import Logger from './services/logger';

Logger.logger.info("Running in Production env: " + Settings.isProduction());

/*Creates DB and models*/
var models = new Models();

/*Creates Basic Initial info*/
SetupData.execute(models);

var app:express.Application = ConfigApp.configure(models);

//ROUTES
app.get('/', function(req, res) {
    models.BlogModel.find(function(err, blogs) {
        res.render('index', {
            blogs: blogs
        });
    });
});

app.get('/about', function(req, res) {
    res.render('about');
});

var adminRoutes = new AdminRoutes(models);
adminRoutes.configure(app);

var blogRoutes = new BlogRoutes(models);
blogRoutes.configure(app);

var productRoutes = new ProductRoutes(models);
productRoutes.configure(app);

var clientRoutes = new ClientRoutes(models);
clientRoutes.configure(app);

var registrationRoutes = new RegistrationRoutes(models);
registrationRoutes.configure(app);

ExceptionRoutes.configure(app);

var server = app.listen(7070, function() {
    Logger.logger.info('Application Running...' + new Date());
    console.log('Application Running...');
});

export default server;
