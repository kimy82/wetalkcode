"use strict";
var setUpData_1 = require('./setUpData');
var modelsBuild_1 = require('./model/modelsBuild');
var adminRoutes_1 = require('./routes/adminRoutes');
var blogRoutes_1 = require('./routes/blogRoutes');
var productRoutes_1 = require('./routes/productRoutes');
var clientRoutes_1 = require('./routes/clientRoutes');
var registrationRoutes_1 = require('./routes/registrationRoutes');
var exceptionRoutes_1 = require('./routes/exceptionRoutes');
var config_1 = require('./configuration/config');
var settings_1 = require('./configuration/settings');
var logger_1 = require('./services/logger');
logger_1.default.logger.info("Running in Production env: " + settings_1.default.isProduction());
var models = new modelsBuild_1.default();
setUpData_1.default.execute(models);
var app = config_1.default.configure(models);
app.get('/', function (req, res) {
    models.BlogModel.find(function (err, blogs) {
        res.render('index', {
            blogs: blogs
        });
    });
});
app.get('/about', function (req, res) {
    res.render('about');
});
var adminRoutes = new adminRoutes_1.default(models);
adminRoutes.configure(app);
var blogRoutes = new blogRoutes_1.default(models);
blogRoutes.configure(app);
var productRoutes = new productRoutes_1.default(models);
productRoutes.configure(app);
var clientRoutes = new clientRoutes_1.default(models);
clientRoutes.configure(app);
var registrationRoutes = new registrationRoutes_1.default(models);
registrationRoutes.configure(app);
exceptionRoutes_1.default.configure(app);
var server = app.listen(7070, function () {
    logger_1.default.logger.info('Application Running...' + new Date());
    console.log('Application Running...');
});
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = server;
//# sourceMappingURL=app.js.map