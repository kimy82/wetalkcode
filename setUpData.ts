import * as moment from 'moment';
import EncryptService from './services/encryptService';
import Models from './model/modelsBuild';
import Logger from './services/logger';

class SetUpData {
    public static execute(models: Models) {
        SetUpData.createsAdmin(models);
        SetUpData.createBlogs(models);
    };

    /**
     * Sets a basic admin user.
     **/
    public static createsAdmin(models: Models) {
        Logger.logger.info("Creating admin user");
        models.UserModel.find({ email: 'wetalkcode@gmail.com' }, function(err, users) {
            if (users.length > 0) {
                for (let user of users) {
                    user.remove();
                }
            }

            var mePro = new models.UserModel({
                username: 'crki1',
                password: EncryptService.createHash('password'),
                email: 'wetalkcode1@gmail.com',
                company: 'We talk Code',
                enabled: true,
                displayName: 'Cristy i Kim 1',
                commentProduct: true,
                commentProductWeb: 'wetalkcode1.co.uk',
                commentProductVersion: 'PRO',
                commentProductLastPaid: new Date(),
                commentProductPayType: 'YEARLY'
            });

            mePro.save();

            var meFree = new models.UserModel({
                username: 'crki2',
                password: EncryptService.createHash('password'),
                email: 'wetalkcode2@gmail.com',
                company: 'We talk Code',
                enabled: true,
                displayName: 'Cristy i Kim 2',
                commentProduct: true,
                commentProductWeb: 'wetalkcode2.co.uk',
                commentProductVersion: 'FREE',
                commentProductLastPaid: new Date(),
                commentProductPayType: 'YEARLY'
            });

            meFree.save();

            var meNone = new models.UserModel({
                username: 'crki0',
                password: EncryptService.createHash('password'),
                email: 'wetalkcode0@gmail.com',
                company: 'We talk Code',
                enabled: true,
                displayName: 'Cristy i Kim 0',
                commentProduct: false,
                commentProductWeb: 'wetalkcode0.co.uk',
                commentProductVersion: 'FREE',
                commentProductLastPaid: new Date(),
                commentProductPayType: 'YEARLY'
            });

            meNone.save();
        });

    }

    /*Removes old existing articles and creates the new ones*/
    public static createBlogs(models: Models) {
        Logger.logger.info("Creating Blogs");
        models.BlogModel.find({}, function(err, blogs) {
            if (blogs.length > 0) {
                for (let blog of blogs) {
                    blog.remove();
                }
            }

            /**
             * Sets the search Engines study.
             **/
            var searchEnginesblog = new models.BlogModel({
                title: "Search Engines",
                tags: "#search",
                body: "Search engines is a study of Solr, Elastic Search and Postgres (Postgis) while performing geospatial querys." +
                " The integration with Spring Data was part of the study.",
                path: "searchEngines",
                published: moment("2016-01-25")
            });
            searchEnginesblog.save();

            /**
             * Sets the Nightwatch study.
             **/
            var nightwatchblog = new models.BlogModel({
                title: "Selenium Nightwatch",
                tags: "#uitest",
                body: "Nightwatch is a selenium framework to run UI testing. The study is proving how easy is to develop tests using Nightwatch" +
                " and if it is possible to integrate it with a CI Pipeline tool such as Jenkins. The test is a simple search in Google showing Nightwatch capabilities.",
                path: "nightwatch",
                published: moment("2016-04-25")
            });
            nightwatchblog.save();

            /**
             * Sets the Autocomplete study.
             **/
            var autocompleteblog = new models.BlogModel({
                title: "Autocomplete",
                tags: "#autocomplete",
                body: "This is an example of an autocomplete functionality with a mantainable dictionary. The example uses Spring framework and camel.",
                path: "autocomplete",
                published: moment("2016-06-25")
            });
            autocompleteblog.save();

            /**
             * Sets the Eureca.io study.
             **/
            var eurecaChat = new models.BlogModel({
                title: "EurecaChat",
                tags: "#chat",
                body: "This is a chat functionality using eureca.io, a nodejs library. Eureca.io is the core engine of the chat. It has been develop using typescript, less and knockout.",
                path: "eurecaChat",
                published: moment("2016-07-01")
            });
            eurecaChat.save();
        });
    }
}

export default SetUpData;
