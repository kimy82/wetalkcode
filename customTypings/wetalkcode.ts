/// <reference path="../typings/express/express.d.ts" />
declare module "wetalkcode" {
    import * as express from 'express';
     module IWeTalkCode {
        export class Configure {
            static configure(app: express.Application): void;
        }
    }
    export = IWeTalkCode;
}
