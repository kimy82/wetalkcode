/**
 * Scroll controlling sticky header
**/
$(window).scroll(function(){
  if($(this).scrollTop() > $('header.navigation').height()){
     $('header.navigation').addClass('sticky');
  }else {
    $('header.navigation').removeClass('sticky');
  }
});

/**
 * Controll the articles dropdown main header
**/
$(function(){
  $('header.navigation li.has-dropdown').click(function(){
    $(this).toggleClass('active');
    $('body').toggleClass('dropdown-open');
  });
  $('body').click(function(e){
    console.log($(e.target));
    if($(this).hasClass('dropdown-open') && !$(e.target).hasClass('has-dropdown') && !$(e.target).parent().hasClass('has-dropdown')){
      $(this).removeClass('dropdown-open');
      $('header.navigation li.active').removeClass('active');
    }
  });
});

/**
 * Controll the articles dropdown hamburguer header
**/
$(function(){
  $('header.navigation li.burguer li.has-dropdown-burguer').click(function(e){
    $('li.has-dropdown-burguer').toggleClass('active');
  });
});

/**
 * Controll the register button
**/
$(function(){
  $('header.navigation li.register').click(function(){
    $(".register-box").fadeToggle();
  });
  $('header.navigation li.register-burguer').click(function(){
    $(".register-box").fadeToggle();
  });



  $('body').click(function(e){
    console.log(e.target);
    if(!$(e.target).hasClass('register') && !$(e.target).parent().hasClass('register') &&
    !$(e.target).hasClass('register-burguer') && !$(e.target).parent().hasClass('register-burguer')
    && !$(e.target).hasClass('register-form')   && !$(e.target).parent().hasClass('register-form') && !$(e.target).parent().parent().hasClass('register-form')
    && !$(e.target).parent().parent().parent().hasClass('register-form')){
      $(".register-box").fadeOut();
    }
  });
});

/**
 * Controll the burguer menu
**/
$(function(){
  $('header.navigation li.burguer').click(function(e){
    if(!$(e.target).hasClass('burguer-menu') && !$(e.target).parent().hasClass('burguer-menu') && !$(e.target).parent().parent().hasClass('burguer-menu')){
      $(".burguer-menu").fadeToggle();
    }
  });

  $('body').click(function(e){
    if(!$(e.target).hasClass('burguer') && !$(e.target).parent().hasClass('burguer') && !$(e.target).parent().parent().hasClass('burguer') && !$(e.target).parent().parent().parent().hasClass('burguer')  ){
      $(".burguer-menu").fadeOut();
    }
  });
});

/**
 * Controller register form
**/
$(function(){
  $("input[type='text']").focus(function(){
    $(this).removeClass("errorInput");
    $(".error").css('display', 'none');
  });

  $(".register-form").submit(function(){
    var error = false;
    if($("#username").val().trim() == ""){
      $("#username").addClass("errorInput");
      error = true;
    }
    if( $("#company").val().trim() == "" ){
      $("#company").addClass("errorInput");
      error = true;
    }
    if($("#email").val().trim() == "" ){
      $("#email").addClass("errorInput");
      error = true;
    }
    if($("#password").val().trim() == "" ){
      $("#password").addClass("errorInput");
      error = true;
    }

    if(error){
      $(".error").css('display', 'block');
      return false;
    }
  });
});
