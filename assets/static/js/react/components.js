var React = require('react');
var ReactDOM = require('react-dom');
var moment = require('moment');

/**
* Controller for comments Block
**/
var commentController = {
  path:"",
  checkCommentForm: function(){
    if($("#new_comment_name").val() == ""){
      $("#new_comment_name").css("border", "1px solid red");
      return;
    }

    if($("#new_comment_body").val()== ""){
        $("#new_comment_body").css("border", "1px solid red");
        return;
    }

    $("#new_comment_name").css("border", "1px solid grey");
    $("#new_comment_body").css("border", "1px solid grey");
  },
  getCommentsUrl : function(){
    return "/blog/comments/" + commentController.path;
  },
  getNewComments: function(){
    $.ajax({
        url: commentController.getCommentsUrl(),
        type: "get",
        success: function(data){
          console.log("new comments");

          ReactDOM.render(
            <CommentBox data={data} />,
            document.getElementById('comments')
          );
        }
    });
  },
  saveComment: function(){
      if($){
        commentController.checkCommentForm();

        var comment = {
          name: $("#new_comment_name").val(),
          body: $("#new_comment_body").val(),
          path: commentController.path
        };

        $.ajax({
            url: "/blog/comment/create",
            type: "post",
            data: comment,
            success: function(){
              console.log("Comment created");
              commentController.getNewComments();
            }
        });
      }else{
        throw "not jquery available. Add jquery before.";
      }
  },
};
/**
* Renders a comment DOM element
*/
var CommentLine = React.createClass({
  render: function() {
    return <div className="comment-box"><p className="name" >@{this.props.data.name} {moment(this.props.date).format("DD MMM YYYY")} commented:</p><p className="comment">{this.props.data.body}</p></div>;
  }
});

/**
* Renders a new comment inputs DOM element
**/
var CommentNewLine = React.createClass({
  render: function() {
    return (
        <div className="new-comment">
          <div className="new-comment-header">
            <input type="text" id="new_comment_name" placeholder="Your name" />
            <a href="javascript: void(0);" onClick={commentController.saveComment}>
              <i className="fa fa-paper-plane-o fa-2x" aria-hidden="true"></i>
            </a>
          </ div>
          <textarea id="new_comment_body" rows="5" placeholder="Your comment" />
        </div>
    );
  }
});

/**
* Renders Comments Block
**/
var CommentBox = React.createClass({
  render: function() {
    return (
      <div className="comments-box">
        <h2>Comments</h2>
        {this.props.data.map(function(comment) {
             return  <CommentLine key={comment._id} data={comment}/>;
          })
        }
        <CommentNewLine />
      </div>
    );
  }
});

$(function(){
  if(path){
      commentController.path = path;
  }else{
      throw "you need to specify a global path for the comments";
  }


  ReactDOM.render(
    <CommentBox data={comments} />,
    document.getElementById('comments')
  );
});
