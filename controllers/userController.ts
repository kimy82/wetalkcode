import Models from '../model/modelsBuild';
import * as moment from 'moment';
import Constants from '../services/constants';
import EncryptService from '../services/encryptService';

class UserController {
    private models: Models;

    constructor(models: Models) {
        this.models = models;
    }

    public createUser(name: string, company: string, email: string, password: string) {
        var client = new this.models.UserModel({
            username: name,
            password: EncryptService.createHash(password),
            email: email,
            company: company,
            enabled: false,
            displayName: name,
            commentProduct: false,
            commentProductWeb: "",
            commentProductVersion: 'FREE_NO_KEY',
            commentProductLastPaid: new Date(),
            commentProductPayType: 'YEARLY'
        });

        client.save();
    }

    /**
     * Updates a user to have a free verison of comments.
     **/
    public updateUserToHaveFreeCommentsProd(email: string) {
        this.findByEmail(email, function(err: Error, users) {
            var user = users[0];
            user.commentProduct = true;
            user.commentProductVersion = 'FREE_NO_KEY';
            user.save();
        });
    }

    /**
     * Updates a user to have a website linked to the comments product.
     **/
    public updateUserCommentProductWeb(email: string, commentProductWeb: string) {
        this.findByEmail(email, function(err: Error, users) {
            var user = users[0];

            user.commentProduct = true;
            if (user.commentProductVersion == "FREE_NO_KEY")
                user.commentProductVersion = 'FREE';
            if (user.commentProductVersion == "PRO_NOT_PAID_NO_KEY")
                user.commentProductVersion = 'PRO_NOT_PAID';
            if (user.commentProductVersion == "PRO_NO_KEY")
                user.commentProductVersion = 'PRO';
            user.commentProductWeb = commentProductWeb;

            user.save();
        });
    }

    /**
     * Updates a user to have a Pro verison of comments but hasn't paid for it yet.
     **/
    public updateUserToHaveProNotPaidCommentsProd(email: string) {
        this.findByEmail(email, function(err: Error, users) {
            var user = users[0];
            user.commentProduct = true;
            user.commentProductVersion = 'PRO_NOT_PAID';
            user.save();
        });
    }

    /**
     * Gets blog comments.
     **/
    public findByEmail(email: string, callback: any) {
        this.models.UserModel.find({
            email: email
        }, callback);
    }

    /**
     * Deletes user.
     **/
    public deleteUser(id: string) {
        this.models.UserModel.remove({
            _id: id
        }, function(err: Error) {
            console.log(err);
        });
    }
}

export default UserController;
