import Models from '../model/modelsBuild';
import * as moment from 'moment';
import Constants from '../services/constants'

class BlogController {
    private models: Models;
 
    constructor(models: Models) {
        this.models = models;
    }

    public saveBlog(title:string, tags:string, body:string, path:string, date:string) {
        if (moment(date, Constants.DATE_FORMAT).isValid()) {
            var blog = new this.models.BlogModel({
                title: title,
                tags: tags,
                body: body,
                path: path,
                published: moment(date, Constants.DATE_FORMAT).toDate()
            });
            blog.save();
        } else {
            console.log('Date is not correct');
            throw 'Date is not correct';
        }

    }

    public deleteBlog(id: string) {
        this.models.BlogModel.remove({
            _id: id
        }, function(err) {
            if(err)
             console.log(err)
        });
    }

    public updateBlog(title:string, tags:string, body:string, path:string, date:string, id:string) {
        if (moment(date, Constants.DATE_FORMAT).isValid()) {
            var blog = this.models.BlogModel.findById(id, function(err, blog) {
                if (err) console.log(err);

                blog.title = title;
                blog.body = body;
                blog.tags = tags;
                blog.path = path;
                blog.published = moment(date, Constants.DATE_FORMAT).toDate();
                blog.save(function(err) {
                    if (err) console.log(err);
                });
            });
        } else {
            console.log('Date is not correct');
            throw 'Date is not correct';
        }
    }

    /**
     * Gets blog comments.
     **/
    public getComments(path: string, callback: any) {
        this.models.CommentModel.find({
            path: path
        }, callback);
    }

    /**
     * Saves a comment.
     **/
    public saveComment(name: string, body: string, path: string) {
        var comment = new this.models.CommentModel({
            name: name,
            body: body,
            path: path,
            date: new Date()
        });
        comment.save();
    }
}

export default BlogController;
