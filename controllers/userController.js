"use strict";
var encryptService_1 = require('../services/encryptService');
var UserController = (function () {
    function UserController(models) {
        this.models = models;
    }
    UserController.prototype.createUser = function (name, company, email, password) {
        var client = new this.models.UserModel({
            username: name,
            password: encryptService_1.default.createHash(password),
            email: email,
            company: company,
            enabled: false,
            displayName: name,
            commentProduct: false,
            commentProductWeb: "",
            commentProductVersion: 'FREE_NO_KEY',
            commentProductLastPaid: new Date(),
            commentProductPayType: 'YEARLY'
        });
        client.save();
    };
    UserController.prototype.updateUserToHaveFreeCommentsProd = function (email) {
        this.findByEmail(email, function (err, users) {
            var user = users[0];
            user.commentProduct = true;
            user.commentProductVersion = 'FREE_NO_KEY';
            user.save();
        });
    };
    UserController.prototype.updateUserCommentProductWeb = function (email, commentProductWeb) {
        this.findByEmail(email, function (err, users) {
            var user = users[0];
            user.commentProduct = true;
            if (user.commentProductVersion == "FREE_NO_KEY")
                user.commentProductVersion = 'FREE';
            if (user.commentProductVersion == "PRO_NOT_PAID_NO_KEY")
                user.commentProductVersion = 'PRO_NOT_PAID';
            if (user.commentProductVersion == "PRO_NO_KEY")
                user.commentProductVersion = 'PRO';
            user.commentProductWeb = commentProductWeb;
            user.save();
        });
    };
    UserController.prototype.updateUserToHaveProNotPaidCommentsProd = function (email) {
        this.findByEmail(email, function (err, users) {
            var user = users[0];
            user.commentProduct = true;
            user.commentProductVersion = 'PRO_NOT_PAID';
            user.save();
        });
    };
    UserController.prototype.findByEmail = function (email, callback) {
        this.models.UserModel.find({
            email: email
        }, callback);
    };
    UserController.prototype.deleteUser = function (id) {
        this.models.UserModel.remove({
            _id: id
        }, function (err) {
            console.log(err);
        });
    };
    return UserController;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = UserController;
//# sourceMappingURL=userController.js.map