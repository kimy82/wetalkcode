"use strict";
var moment = require('moment');
var constants_1 = require('../services/constants');
var BlogController = (function () {
    function BlogController(models) {
        this.models = models;
    }
    BlogController.prototype.saveBlog = function (title, tags, body, path, date) {
        if (moment(date, constants_1.default.DATE_FORMAT).isValid()) {
            var blog = new this.models.BlogModel({
                title: title,
                tags: tags,
                body: body,
                path: path,
                published: moment(date, constants_1.default.DATE_FORMAT).toDate()
            });
            blog.save();
        }
        else {
            console.log('Date is not correct');
            throw 'Date is not correct';
        }
    };
    BlogController.prototype.deleteBlog = function (id) {
        this.models.BlogModel.remove({
            _id: id
        }, function (err) {
            if (err)
                console.log(err);
        });
    };
    BlogController.prototype.updateBlog = function (title, tags, body, path, date, id) {
        if (moment(date, constants_1.default.DATE_FORMAT).isValid()) {
            var blog = this.models.BlogModel.findById(id, function (err, blog) {
                if (err)
                    console.log(err);
                blog.title = title;
                blog.body = body;
                blog.tags = tags;
                blog.path = path;
                blog.published = moment(date, constants_1.default.DATE_FORMAT).toDate();
                blog.save(function (err) {
                    if (err)
                        console.log(err);
                });
            });
        }
        else {
            console.log('Date is not correct');
            throw 'Date is not correct';
        }
    };
    BlogController.prototype.getComments = function (path, callback) {
        this.models.CommentModel.find({
            path: path
        }, callback);
    };
    BlogController.prototype.saveComment = function (name, body, path) {
        var comment = new this.models.CommentModel({
            name: name,
            body: body,
            path: path,
            date: new Date()
        });
        comment.save();
    };
    return BlogController;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = BlogController;
//# sourceMappingURL=blogController.js.map