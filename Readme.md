##Run project Instructions
* _node app.js_
* _gulp browserify_ (wach for changes in components.js and creates a raw js file in build folder)
* Install less  "npm install -g less" and then _gulp less_

##Using nginx
** We use Nginx as a load balancer between wetalkcode and some other applications**
* _sudo apt-get update_
* _sudo apt-get install nginx_
* go to etc/nginx/sites-available.
* Edit default file and place the configuration we have in default.
* Once changed reload configuration with _sudo nginx -s reload_
* If server wasn't started use _sudo service nginx restart_

##Redis
** We use Redis for SSO(Single sign on). So iut stores the session of the user in Central point accross al applications.
* _sudo apt-get install redis-server_
* Start redis server with _redis-server &_
* _ps -ef | grep redis_ doing this you can check redis is running

##DocKer Instructions
* _docker build -t kimy82/wtc ._
* _docker login --username= --email=_
* _docker push kimy82/wtc_
* _docker run -p 8080:8080 kimy82/wtc_

##Docker useful comments
* _docker ps_ (view active images)
* _docker ps -a_ (view all images)
* remove image
    * _docker stop pid_
    * _docker rm pid_
* Pul image from repo**
    * _docker pull kimy82/wtc_
* Check wetalkcode log
    * _docker cp dd8d057a70e3:/usr/src/wtc/wetalkcode.log /_
