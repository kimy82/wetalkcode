var fs = require('fs');

class Settings{
  private static env:string = 'production';
  static getEnv():string {
    var settingsJson = JSON.parse(fs.readFileSync('settings.json', 'utf8'));
    return settingsJson.env;
  }

  static setEnvToDevelopment():void{
    var settingsJson = JSON.parse(fs.readFileSync('settings.json', 'utf8'));
    settingsJson.env = 'development';
    fs.writeFile('settings.json', JSON.stringify(settingsJson), function(err) {
        if(err) {
            return console.log(err);
        }

        console.log("The file was saved!");
    });
  }

  static setEnvToProduction():void{
    var settingsJson = JSON.parse(fs.readFileSync('settings.json', 'utf8'));
    settingsJson.env = 'production';
    fs.writeFile('settings.json', JSON.stringify(settingsJson), function(err) {
        if(err) {
            return console.log(err);
        }

        console.log("The file was saved!");
    });
  }

  static isProduction():boolean{
    return Settings.getEnv() === 'production';
  }

  static getDomain():string{
    var settingsJson = JSON.parse(fs.readFileSync('settings.json', 'utf8'));
    if( Settings.getEnv() === 'production'){
      return settingsJson.prod.domain;
    } else {
      return settingsJson.local.domain;
    }
  }
}

export default Settings;
