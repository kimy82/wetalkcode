import * as passport from 'passport';
import Models from '../model/modelsBuild';
import EncryptService from '../services/encryptService';
import * as User from '../model/user';
import * as express from 'express';
import * as  mongoose from 'mongoose';
import Settings from './settings';
import Logger from '../services/logger';

var Strategy = require('passport-local').Strategy;


class ConfigPassport {
  public userModel:mongoose.Model<User.IUserDocument>;

  constructor(){
    this.userModel =  new Models().UserModel;
  }

  public configure(passport: passport.Passport) {

        passport.use(new Strategy(
            (email: string, password: string, cb) => {
              Logger.logger.info("User with email " + email + "is loggin in.");
              this.userModel.findOne({ email: email }, function(err, user) {
                    if (EncryptService.isValid(password, user.password)) {
                        Logger.logger.info("User with email " + email + "is logged in.");
                        return cb(null, user);
                    } else {
                        return cb(null, false);
                    }
                });
            }));

        passport.serializeUser(function(user: User.IUserDocument, cb) {
            cb(null, user._id);
        });

        passport.deserializeUser((id, cb) => {
            this.userModel.find({ _id: id }, function(err, user: User.IUserDocument) {
                cb(null, user);
            });
        });
    };

    public addLoginRoutes(app: express.Application) {
        //LOGIN PAGE
        app.get('/login', function(req, res) {
            res.render('login');
        });

        //LOGIN ENDPOINT
        app.post('/login', passport.authenticate('local', {
            successReturnToOrRedirect: '/dashboard',
            failureRedirect: '/login',
            failureFlash: false
        })
        );

        //LOGOUT
        app.get('/logout', function(req, res) {
            req.logout();
            res.redirect('/');
        });
    }
}

export default ConfigPassport;
