"use strict";
var fs = require('fs');
var Settings = (function () {
    function Settings() {
    }
    Settings.getEnv = function () {
        var settingsJson = JSON.parse(fs.readFileSync('settings.json', 'utf8'));
        return settingsJson.env;
    };
    Settings.setEnvToDevelopment = function () {
        var settingsJson = JSON.parse(fs.readFileSync('settings.json', 'utf8'));
        settingsJson.env = 'development';
        fs.writeFile('settings.json', JSON.stringify(settingsJson), function (err) {
            if (err) {
                return console.log(err);
            }
            console.log("The file was saved!");
        });
    };
    Settings.setEnvToProduction = function () {
        var settingsJson = JSON.parse(fs.readFileSync('settings.json', 'utf8'));
        settingsJson.env = 'production';
        fs.writeFile('settings.json', JSON.stringify(settingsJson), function (err) {
            if (err) {
                return console.log(err);
            }
            console.log("The file was saved!");
        });
    };
    Settings.isProduction = function () {
        return Settings.getEnv() === 'production';
    };
    Settings.getDomain = function () {
        var settingsJson = JSON.parse(fs.readFileSync('settings.json', 'utf8'));
        if (Settings.getEnv() === 'production') {
            return settingsJson.prod.domain;
        }
        else {
            return settingsJson.local.domain;
        }
    };
    Settings.env = 'production';
    return Settings;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Settings;
//# sourceMappingURL=settings.js.map