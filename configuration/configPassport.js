"use strict";
var passport = require('passport');
var modelsBuild_1 = require('../model/modelsBuild');
var encryptService_1 = require('../services/encryptService');
var logger_1 = require('../services/logger');
var Strategy = require('passport-local').Strategy;
var ConfigPassport = (function () {
    function ConfigPassport() {
        this.userModel = new modelsBuild_1.default().UserModel;
    }
    ConfigPassport.prototype.configure = function (passport) {
        var _this = this;
        passport.use(new Strategy(function (email, password, cb) {
            logger_1.default.logger.info("User with email " + email + "is loggin in.");
            _this.userModel.findOne({ email: email }, function (err, user) {
                if (encryptService_1.default.isValid(password, user.password)) {
                    logger_1.default.logger.info("User with email " + email + "is logged in.");
                    return cb(null, user);
                }
                else {
                    return cb(null, false);
                }
            });
        }));
        passport.serializeUser(function (user, cb) {
            cb(null, user._id);
        });
        passport.deserializeUser(function (id, cb) {
            _this.userModel.find({ _id: id }, function (err, user) {
                cb(null, user);
            });
        });
    };
    ;
    ConfigPassport.prototype.addLoginRoutes = function (app) {
        app.get('/login', function (req, res) {
            res.render('login');
        });
        app.post('/login', passport.authenticate('local', {
            successReturnToOrRedirect: '/dashboard',
            failureRedirect: '/login',
            failureFlash: false
        }));
        app.get('/logout', function (req, res) {
            req.logout();
            res.redirect('/');
        });
    };
    return ConfigPassport;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = ConfigPassport;
//# sourceMappingURL=configPassport.js.map