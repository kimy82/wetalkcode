"use strict";
var sitemap = require('express-sitemap');
var bodyParser = require('body-parser');
var session = require('express-session');
var redis = require("redis");
var redisStore = require('connect-redis')(session);
var redisClient = redis.createClient(6379, '172.18.0.25');
var configPassport_1 = require('./configPassport');
var siteMapConfig_1 = require('./siteMapConfig');
var express = require('express');
var passport = require('passport');
var logger_1 = require('../services/logger');
var settings_1 = require('./settings');
var ConfigApp = (function () {
    function ConfigApp() {
    }
    ConfigApp.configure = function (models) {
        logger_1.default.logger.info("Configuration Wetalkcode...");
        var configPassport = new configPassport_1.default();
        configPassport.configure(passport);
        var app = express();
        app.set('views', 'assets');
        app.use(express.static('assets/static'));
        app.set('view engine', 'jade');
        app.use(bodyParser.urlencoded({ extended: true }));
        app.use(session({
            secret: 'sosecret',
            store: new redisStore({ host: '172.18.0.25', port: 6379, client: redisClient, ttl: 260 }),
            saveUninitialized: false,
            resave: false,
            cookie: { expires: false, domain: settings_1.default.getDomain(), maxAge: 30 * 24 * 60 * 60 * 1000 }
        }));
        app.use(passport.initialize());
        app.use(passport.session());
        app.locals.moment = require('moment');
        app.use(function (req, res, next) {
            res.locals.login = req.isAuthenticated();
            res.locals.user = req.user;
            next();
        });
        configPassport.addLoginRoutes(app);
        siteMapConfig_1.default.configure(app);
        return app;
    };
    return ConfigApp;
}());
exports.config = ConfigApp;
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = ConfigApp;
//# sourceMappingURL=config.js.map