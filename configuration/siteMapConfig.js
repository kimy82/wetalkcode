"use strict";
var sitemap = require('express-sitemap');
var SiteMapConfig = (function () {
    function SiteMapConfig() {
    }
    SiteMapConfig.configure = function (app) {
        var weTalkCodeSiteMap = sitemap({
            url: 'www.wetalkcode.co.uk',
            map: {
                '/': ['get'],
                '/about': ['get'],
                '/blog/view/searchEngines': ['get'],
                '/blog/view/nightwatch': ['get'],
                '/blog/view/autocomplete': ['get'],
                '/blog/view/eurecaChat': ['get'],
                '/admin': ['get'],
                '/login': ['get'],
            },
            route: {
                '/': {
                    lastmod: '2016-07-05',
                    changefreq: 'always',
                    priority: 1.0,
                },
                '/about': {
                    lastmod: '2016-07-05',
                    changefreq: 'always',
                    priority: 1.0,
                },
                '/blog/view/searchEngines': {
                    lastmod: '2016-07-05',
                    changefreq: 'always',
                    priority: 1.0,
                },
                '/blog/view/autocomplete': {
                    lastmod: '2016-07-05',
                    changefreq: 'always',
                    priority: 1.0,
                },
                '/blog/view/eurecaChat': {
                    lastmod: '2016-07-05',
                    changefreq: 'always',
                    priority: 1.0,
                },
                '/admin': {
                    disallow: true,
                    hide: true,
                },
                '/login': {
                    hide: true,
                    disallow: true,
                },
            },
        });
        weTalkCodeSiteMap.toFile();
        app.get('/sitemap.xml', function (req, res) {
            weTalkCodeSiteMap.XMLtoWeb(res);
        });
        app.get('/robots.txt', function (req, res) {
            weTalkCodeSiteMap.TXTtoWeb(res);
        });
    };
    return SiteMapConfig;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = SiteMapConfig;
//# sourceMappingURL=siteMapConfig.js.map