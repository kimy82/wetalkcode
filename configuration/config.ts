var sitemap = require('express-sitemap');
var bodyParser = require('body-parser');
var session = require('express-session');
var redis = require("redis");
var redisStore = require('connect-redis')(session);
var redisClient = redis.createClient(6379, '172.18.0.25');

import ConfigPassport from './configPassport';
import SiteMapConfig from './siteMapConfig';
import * as express from 'express';
import * as passport from 'passport';
import Models from '../model/modelsBuild';
import Logger from '../services/logger';
import Settings from './settings';



class ConfigApp {
    static configure(models: Models):express.Application {
        Logger.logger.info("Configuration Wetalkcode...");
        var configPassport = new ConfigPassport();
        configPassport.configure(passport);
        //EXPRESS APP//
        var app = express();

        app.set('views', 'assets');
        app.use(express.static('assets/static'));
        app.set('view engine', 'jade');
        app.use(bodyParser.urlencoded({ extended: true }));
        app.use(session({
            secret: 'sosecret',
            store: new redisStore({ host: '172.18.0.25', port: 6379, client: redisClient, ttl: 260 }),
            saveUninitialized: false,
            resave: false,
            cookie: { expires: false, domain: Settings.getDomain(), maxAge: 30 * 24 * 60 * 60 * 1000 }
        }));
        app.use(passport.initialize());
        app.use(passport.session());
        app.locals.moment = require('moment'); /* moment available in Jade */

        /**
         * Make user available.
        **/
        app.use(function(req, res, next) {
            res.locals.login = req.isAuthenticated();
            res.locals.user = req.user;
            next();
        });

        configPassport.addLoginRoutes(app);
        //EXPRESS APP END//

        SiteMapConfig.configure(app);

        return app;
    }
}

export var config = ConfigApp;
export default ConfigApp;
