##Creating the wetalkcode network
docker network create --subnet=172.18.0.0/16 wetalkcodenet
##Start redis
docker run --net wetalkcodenet --ip 172.18.0.25 redis
##Start mongodb
docker run --net wetalkcodenet --ip 172.18.0.21 -p 27017:27017 kimy82/mongodb
##Start nginx
docker run --net wetalkcodenet --ip 172.18.0.24 -p 8080:80 kimy82/nginx
##Start wetalkcode
docker run --net wetalkcodenet --ip 172.18.0.22 -p 7070:7070 kimy82/wtc
##Start wetalk
docker run --net wetalkcodenet --ip 172.18.0.23 -p 6060:6060 kimy82/commentwtc
