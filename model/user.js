"use strict";
var mongoose = require('mongoose');
var constants_1 = require('../services/constants');
var User = (function () {
    function User() {
    }
    User.schema = new mongoose.Schema({
        username: String,
        password: String,
        email: String,
        company: String,
        enabled: Boolean,
        displayName: String,
        commentProduct: Boolean,
        commentProductWeb: String,
        commentProductVersion: {
            type: String,
            default: null,
            enum: constants_1.default.PRODUCT_TYPES,
        },
        commentProductLastPaid: Date,
        commentProductPayType: {
            type: String,
            default: null,
            enum: constants_1.default.PAYMENT_TYPES,
        },
    });
    return User;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = User;
//# sourceMappingURL=user.js.map