"use strict";
var mongoose = require('mongoose');
var Blog = (function () {
    function Blog() {
    }
    Blog.schema = new mongoose.Schema({
        title: String,
        body: String,
        tags: String,
        path: String,
        published: Date
    });
    return Blog;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Blog;
//# sourceMappingURL=blog.js.map