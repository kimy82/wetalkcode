"use strict";
var mongoose = require('mongoose');
var Comment = (function () {
    function Comment() {
    }
    Comment.schema = new mongoose.Schema({
        name: String,
        body: String,
        path: String,
        date: Date,
    });
    return Comment;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Comment;
//# sourceMappingURL=comment.js.map