import * as  mongoose from 'mongoose';
import * as Blog from './blog';
import * as User from './user';
import * as Comment from './comment';

class Models {

    public BlogModel: mongoose.Model<Blog.IBlogDocument>;
    public UserModel: mongoose.Model<User.IUserDocument>;
    public CommentModel: mongoose.Model<Comment.ICommentDocument>;
    public connection: mongoose.Connection;

    constructor() {
        this.connection = mongoose.createConnection('mongodb://172.18.0.21/wtc', { server: { reconnectTries: Number.MAX_VALUE } });
        this.connection.on('error', console.error.bind(console, 'connection error:'));
        this.connection.once('open', function() {
            console.log("Connected to mongoDB");
        });
        this.UserModel = this.connection.model<User.IUserDocument>('User', User.default.schema);
        this.BlogModel =this.connection.model<Blog.IBlogDocument>('Blog', Blog.default.schema);
        this.CommentModel = this.connection.model<Comment.ICommentDocument>('Comment', Comment.default.schema);
    }
}

export default Models;
