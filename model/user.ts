import * as  mongoose from 'mongoose';
import Constants from '../services/constants';

export interface IUserDocument extends mongoose.Document {
  username: string,
  password: string,
  email: string,
  company: string,
  enabled: boolean,
  displayName: string,
  commentProduct: boolean,
  commentProductWeb: string,
  commentProductVersion:string,
  commentProductLastPaid: Date,
  commentProductPayType: string,
}

class User {
    public static schema: mongoose.Schema = new mongoose.Schema({
        username: String,
        password: String,
        email: String,
        company: String,
        enabled: Boolean,
        displayName: String,
        commentProduct: Boolean,
        commentProductWeb: String,
        commentProductVersion: {
            type: String,
            default: null,
            enum: Constants.PRODUCT_TYPES,
        },
        commentProductLastPaid: Date,
        commentProductPayType: {
            type: String,
            default: null,
            enum: Constants.PAYMENT_TYPES,
        },
    });
}

export default User;
