import * as  mongoose from 'mongoose';

export interface ICommentDocument extends mongoose.Document {
  name: string,
  body: string,
  path: string,
  date: Date
}

class Comment {
    public static schema: mongoose.Schema = new mongoose.Schema({
      name: String,
      body: String,
      path: String,
      date: Date,
    });
}

export default Comment;
