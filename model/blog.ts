import * as  mongoose from 'mongoose';

export interface IBlogDocument extends mongoose.Document {
    title: string;
    body: string;
    tags: string;
    path: string;
    published: Date;
}

class Blog {
    public static schema: mongoose.Schema = new mongoose.Schema({
      title: String,
      body: String,
      tags: String,
      path: String,
      published: Date
    });
}

export default Blog;
