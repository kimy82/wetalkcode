"use strict";
var mongoose = require('mongoose');
var Blog = require('./blog');
var User = require('./user');
var Comment = require('./comment');
var Models = (function () {
    function Models() {
        this.connection = mongoose.createConnection('mongodb://172.18.0.21/wtc', { server: { reconnectTries: Number.MAX_VALUE } });
        this.connection.on('error', console.error.bind(console, 'connection error:'));
        this.connection.once('open', function () {
            console.log("Connected to mongoDB");
        });
        this.UserModel = this.connection.model('User', User.default.schema);
        this.BlogModel = this.connection.model('Blog', Blog.default.schema);
        this.CommentModel = this.connection.model('Comment', Comment.default.schema);
    }
    return Models;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Models;
//# sourceMappingURL=modelsBuild.js.map