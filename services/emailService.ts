import * as moment from 'moment';
var nodemailer = require('nodemailer');
import EncryptService from '../services/encryptService';
import Settings from '../configuration/settings';
import Logger from '../services/logger';

class EmailService {
  public transporter;
  constructor(){
      this.transporter = nodemailer.createTransport('smtps://wetalkcode@gmail.com:tenerife2015@smtp.gmail.com');
  }

  public sendEmailVerification(name:string, company: string, email:string, password: string, url: string) {
    var mailOptions = {
        from: '"We Talk Code" <wetalkcode@gmail.com>', // sender address
        to: email,
        subject: 'WTC Email verification ✔', // Subject line
        html: 'Hi <b>' + name + '</b> from <b>' + company + ', </b><br>We Talk Code need an email verification. <br>'
        + 'Follow the link below to verify your account: <br>'
        + ' http://' + url + '/verify/' + email + '?key=' + EncryptService.createHash(name + company)
    };
    
    //if(Settings.isProduction()){
      // send mail with defined transport object
      Logger.logger.info("Sending registration email to : " + name + " " + email);
      this.transporter.sendMail(mailOptions, function(error: Error, info){
          if(error){
              Logger.logger.error("Sending registration email failed : " + error);
              return console.log(error);
          }
          Logger.logger.error("Email Sent : " + info.response);
      });
  //}

  }
}

export default EmailService;
