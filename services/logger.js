"use strict";
var winston = require('winston');
var Logger = (function () {
    function Logger() {
    }
    Logger.logger = new winston.Logger({
        exitOnError: false,
        level: 'info',
        transports: [
            new (winston.transports.File)({ filename: 'wetalkcode.log' })
        ]
    });
    return Logger;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Logger;
//# sourceMappingURL=logger.js.map