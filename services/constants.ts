class Constants {
  static PRODUCT_TYPES = ['FREE', 'FREE_NO_KEY', 'PRO', 'PRO_NOT_PAID_NO_KEY', 'PRO_NOT_PAID', 'PRO_NO_KEY'];
  static PAYMENT_TYPES = ['YEARLY', 'MONTHLY'];
  static DATE_FORMAT:string = "YYYY-MM-DD";
  static PRODUCT_TYPE_FREE:string = "free";
  static PRODUCT_TYPE_PRO:string = "pro";
}

export default Constants;
