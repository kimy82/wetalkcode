"use strict";
var nodemailer = require('nodemailer');
var encryptService_1 = require('../services/encryptService');
var logger_1 = require('../services/logger');
var EmailService = (function () {
    function EmailService() {
        this.transporter = nodemailer.createTransport('smtps://wetalkcode@gmail.com:tenerife2015@smtp.gmail.com');
    }
    EmailService.prototype.sendEmailVerification = function (name, company, email, password, url) {
        var mailOptions = {
            from: '"We Talk Code" <wetalkcode@gmail.com>',
            to: email,
            subject: 'WTC Email verification ✔',
            html: 'Hi <b>' + name + '</b> from <b>' + company + ', </b><br>We Talk Code need an email verification. <br>'
                + 'Follow the link below to verify your account: <br>'
                + ' http://' + url + '/verify/' + email + '?key=' + encryptService_1.default.createHash(name + company)
        };
        logger_1.default.logger.info("Sending registration email to : " + name + " " + email);
        this.transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                logger_1.default.logger.error("Sending registration email failed : " + error);
                return console.log(error);
            }
            logger_1.default.logger.error("Email Sent : " + info.response);
        });
    };
    return EmailService;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = EmailService;
//# sourceMappingURL=emailService.js.map