var winston = require('winston');

class Logger {
    static logger: any = new winston.Logger({
        exitOnError: false,
        level: 'info',
        transports: [
            new (winston.transports.File)({ filename: 'wetalkcode.log' })
        ]
    });
}

export default Logger;
