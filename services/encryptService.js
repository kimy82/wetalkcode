"use strict";
var bCrypt = require('bcrypt-nodejs');
var EncryptService = (function () {
    function EncryptService() {
    }
    EncryptService.createHash = function (toEncrypt) {
        return bCrypt.hashSync(toEncrypt, bCrypt.genSaltSync(10));
    };
    EncryptService.isValid = function (hash1, hash2) {
        return bCrypt.compareSync(hash1, hash2);
    };
    return EncryptService;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = EncryptService;
//# sourceMappingURL=encryptService.js.map