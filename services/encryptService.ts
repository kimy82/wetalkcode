import * as bCrypt from 'bcrypt-nodejs'

class EncryptService {
  static createHash(toEncrypt:string):string {
    return bCrypt.hashSync(toEncrypt, bCrypt.genSaltSync(10));
  }

  static isValid(hash1, hash2):boolean {
    return bCrypt.compareSync(hash1, hash2);
  }
}

export default EncryptService;
