"use strict";
var Constants = (function () {
    function Constants() {
    }
    Constants.PRODUCT_TYPES = ['FREE', 'FREE_NO_KEY', 'PRO', 'PRO_NOT_PAID_NO_KEY', 'PRO_NOT_PAID', 'PRO_NO_KEY'];
    Constants.PAYMENT_TYPES = ['YEARLY', 'MONTHLY'];
    Constants.DATE_FORMAT = "YYYY-MM-DD";
    Constants.PRODUCT_TYPE_FREE = "free";
    Constants.PRODUCT_TYPE_PRO = "pro";
    return Constants;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Constants;
//# sourceMappingURL=constants.js.map