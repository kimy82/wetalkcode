"use strict";
var logger_1 = require('../services/logger');
var ExceptionRoutes = (function () {
    function ExceptionRoutes() {
    }
    ExceptionRoutes.configure = function (app) {
        app.use(function (err, req, res, next) {
            logger_1.default.logger.error("Error " + err);
            res.status(500);
            res.render('error');
        });
        app.use(function (req, res, next) {
            res.status(404);
            if (req.accepts('html')) {
                res.render('404');
                return;
            }
            if (req.accepts('json')) {
                res.send({ error: 'Not found' });
                return;
            }
        });
    };
    return ExceptionRoutes;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = ExceptionRoutes;
//# sourceMappingURL=exceptionRoutes.js.map