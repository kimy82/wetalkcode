import * as express from "express";
import Models from '../model/modelsBuild';
import UserController from '../controllers/userController';
import EncryptService from '../services/encryptService';
import Constants from '../services/constants';
import Logger from '../services/logger';


class ProductRoutes{
  public models:Models;
  public userController:UserController

  constructor(models: Models){
    this.models = models;
    this.userController = new UserController(models);
  }

  public configure(app: express.Application){
    app.get('/product/commentswtc', this.renderCommentProductMainPage);

    app.get('/product/commentswtc/get/:type', require('connect-ensure-login').ensureLoggedIn(), this.getCommentProductByType);

    app.post('/product/generateKey/:theweb', require('connect-ensure-login').ensureLoggedIn(), this.generateKeyForProduct);
  }

  public generateKeyForProduct = (req: express.Request, res: express.Response) => {
    var user = req.user[0];
    var theweb = req.params.theweb;
    var key = "";
    Logger.logger.info("Generating key for product for user " + user.username + " and web " + theweb);
    if(user.commentProduct){
      this.userController.updateUserCommentProductWeb(user.email, theweb);
      key = EncryptService.createHash(user.email + theweb);
      Logger.logger.info("Generated the key for product for user " + user.username + " and web " + theweb);
      res.json({status: 'success', key:  key});
    }else {
      Logger.logger.error("The key for product for user " + user.username + " and web " + theweb + " haven't been generated");
      res.json({status: 'failed', reason: 'No key could be generated!. You haven\'t choosen the product yet. Please get it first'});
    }
  }

  public renderCommentProductMainPage  = (req: express.Request, res: express.Response) => {
        res.render('products/commentswtc');
  }

  public getCommentProductByType = (req: express.Request, res: express.Response) => {
    var type = req.params.type;
    if(type === Constants.PRODUCT_TYPE_FREE){
      this.userController.updateUserToHaveFreeCommentsProd(req.user[0].email);
    } else if (type === Constants.PRODUCT_TYPE_PRO){
      this.userController.updateUserToHaveProNotPaidCommentsProd(req.user[0].email);
    } else {
      Logger.logger.error("The type" + type + " is not recognised as a product type");
      res.render("errorPage",{errorMessage: 'User could not be verified! try again...'});
      return;
    }

    res.render('products/getcommentswtc', {
      type: req.params.type
    });
  }
}
export default ProductRoutes;
