"use strict";
var ClientRoutes = (function () {
    function ClientRoutes(models) {
        this.renderClientDashboard = function (req, res) {
            res.render('client/dashboard');
        };
        this.models = models;
    }
    ClientRoutes.prototype.configure = function (app) {
        app.get('/dashboard', require('connect-ensure-login').ensureLoggedIn(), this.renderClientDashboard);
    };
    return ClientRoutes;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = ClientRoutes;
//# sourceMappingURL=clientRoutes.js.map