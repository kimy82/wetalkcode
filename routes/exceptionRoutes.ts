import * as express from "express";
import Models from '../model/modelsBuild';
import BlogController from '../controllers/blogController';
import Logger from '../services/logger';

class ExceptionRoutes {
    static configure(app: express.Application) {
        /**
         * Captures any exception in the code.
         **/
        app.use(function(err: Error, req: express.Request, res: express.Response, next) {
            Logger.logger.error("Error " + err);
            res.status(500);
            res.render('error');
        });

        app.use(function(req: express.Request, res: express.Response, next) {
            res.status(404);

            // respond with html page
            if (req.accepts('html')) {
                res.render('404');
                return;
            }

            // respond with json
            if (req.accepts('json')) {
                res.send({ error: 'Not found' });
                return;
            }
        });
    }
}

export default ExceptionRoutes;
