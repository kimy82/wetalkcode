import * as express from "express";
import Models from '../model/modelsBuild';
import UserController from '../controllers/userController';
import EncryptService from '../services/encryptService';
import Constants from '../services/constants';


class ClientRoutes {
    public models: Models;

    constructor(models: Models) {
        this.models = models;
    }

    public configure(app: express.Application) {
        app.get('/dashboard', require('connect-ensure-login').ensureLoggedIn(), this.renderClientDashboard);
    }

    public renderClientDashboard = (req: express.Request, res: express.Response) => {
        res.render('client/dashboard');
    }
}

export default ClientRoutes;
