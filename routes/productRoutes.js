"use strict";
var userController_1 = require('../controllers/userController');
var encryptService_1 = require('../services/encryptService');
var constants_1 = require('../services/constants');
var logger_1 = require('../services/logger');
var ProductRoutes = (function () {
    function ProductRoutes(models) {
        var _this = this;
        this.generateKeyForProduct = function (req, res) {
            var user = req.user[0];
            var theweb = req.params.theweb;
            var key = "";
            logger_1.default.logger.info("Generating key for product for user " + user.username + " and web " + theweb);
            if (user.commentProduct) {
                _this.userController.updateUserCommentProductWeb(user.email, theweb);
                key = encryptService_1.default.createHash(user.email + theweb);
                logger_1.default.logger.info("Generated the key for product for user " + user.username + " and web " + theweb);
                res.json({ status: 'success', key: key });
            }
            else {
                logger_1.default.logger.error("The key for product for user " + user.username + " and web " + theweb + " haven't been generated");
                res.json({ status: 'failed', reason: 'No key could be generated!. You haven\'t choosen the product yet. Please get it first' });
            }
        };
        this.renderCommentProductMainPage = function (req, res) {
            res.render('products/commentswtc');
        };
        this.getCommentProductByType = function (req, res) {
            var type = req.params.type;
            if (type === constants_1.default.PRODUCT_TYPE_FREE) {
                _this.userController.updateUserToHaveFreeCommentsProd(req.user[0].email);
            }
            else if (type === constants_1.default.PRODUCT_TYPE_PRO) {
                _this.userController.updateUserToHaveProNotPaidCommentsProd(req.user[0].email);
            }
            else {
                logger_1.default.logger.error("The type" + type + " is not recognised as a product type");
                res.render("errorPage", { errorMessage: 'User could not be verified! try again...' });
                return;
            }
            res.render('products/getcommentswtc', {
                type: req.params.type
            });
        };
        this.models = models;
        this.userController = new userController_1.default(models);
    }
    ProductRoutes.prototype.configure = function (app) {
        app.get('/product/commentswtc', this.renderCommentProductMainPage);
        app.get('/product/commentswtc/get/:type', require('connect-ensure-login').ensureLoggedIn(), this.getCommentProductByType);
        app.post('/product/generateKey/:theweb', require('connect-ensure-login').ensureLoggedIn(), this.generateKeyForProduct);
    };
    return ProductRoutes;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = ProductRoutes;
//# sourceMappingURL=productRoutes.js.map