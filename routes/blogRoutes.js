"use strict";
var blogController_1 = require('../controllers/blogController');
var BlogRoutes = (function () {
    function BlogRoutes(models) {
        var _this = this;
        this.viewBlog = function (req, res) {
            _this.blogController.getComments(req.params.path, function (err, comments) {
                res.render('blogs/' + req.params.path + "/" + req.params.path, {
                    comments: JSON.stringify(comments).replace(/[\/\\]/g, '_'),
                    path: req.params.path
                });
            });
        };
        this.getBlogComments = function (req, res) {
            _this.blogController.getComments(req.params.path, function (err, comments) {
                res.json(comments);
            });
        };
        this.createBlogComment = function (req, res) {
            _this.blogController.saveComment(req.body.name, req.body.body, req.body.path);
            res.json({ status: 'success' });
        };
        this.models = models;
        this.blogController = new blogController_1.default(this.models);
    }
    BlogRoutes.prototype.configure = function (app) {
        app.get('/blog/view/:path', this.viewBlog);
        app.get('/blog/comments/:path', this.getBlogComments);
        app.post('/blog/comment/create', this.createBlogComment);
    };
    return BlogRoutes;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = BlogRoutes;
//# sourceMappingURL=blogRoutes.js.map