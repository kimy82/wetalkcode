import * as express from "express";
import Models from '../model/modelsBuild';
import BlogController from '../controllers/blogController';
import Logger from '../services/logger';

class AdminRoutes {
    public models: Models;
    public blogController: BlogController;

    constructor(models: Models) {
        this.models = models;
        this.blogController = new BlogController(this.models);
    }

    public configure(app: express.Application) {
        app.get('/admin/blogs', require('connect-ensure-login').ensureLoggedIn(), this.showBlogs);
        app.post('/admin/blog', require('connect-ensure-login').ensureLoggedIn(), this.editCreateBlog);
        app.get('/admin/blog', require('connect-ensure-login').ensureLoggedIn(), this.goCreateBlog);
        app.get('/admin/blog/delete/:id', require('connect-ensure-login').ensureLoggedIn(), this.deleteBlog);
        app.get('/admin/blog/edit/:id', require('connect-ensure-login').ensureLoggedIn(), this.showBlog);
    };

    public showBlogs = (req: express.Request, res: express.Response) => {
        this.models.BlogModel.find(function(err, blogs) {
            res.render('admin/blogs', {
                blogs: blogs
            });
        });
    }

    public showBlog = (req: express.Request, res: express.Response) => {
      this.models.BlogModel.findById(req.params.id, function(err, blog) {
          res.render('admin/blog', {
              blog: blog
          });
      });
    }

    public goCreateBlog = (req: express.Request, res: express.Response) => {
      var blog = new this.models.BlogModel({title: "", body: "", tags: "", path: ""});
      res.render('admin/blog', {
          blog: blog
      });
    }

    public editCreateBlog = (req: express.Request, res: express.Response) => {

        if(req.body.title === undefined || req.body.tags === undefined || req.body.body === undefined || req.body.published === undefined){
          Logger.logger.error("Error: No form data for Blog creation");
          throw new Error("No form data for Blog creation");
        }

        if (req.body.id != "" && req.body.id !== undefined) {
            Logger.logger.info("Updating blog " + req.body.path);
            this.blogController.updateBlog(req.body.title, req.body.tags, req.body.body, req.body.path, req.body.published, req.body.id);
        } else {
            Logger.logger.info("Saving blog " + req.body.path);
            this.blogController.saveBlog(req.body.title, req.body.tags, req.body.body, req.body.path, req.body.published);
        }

        var blog = new this.models.BlogModel({ title: "", body: "", tags: "", path: "" });

        res.render('admin/blog', {
            blog: blog
        });
    }

    public deleteBlog = (req: express.Request, res: express.Response) => {
      Logger.logger.info("Deleting blog " + req.params.id);
      this.blogController.deleteBlog(req.params.id);
      res.json({status: 'success'});
    }
}
export default AdminRoutes;
