"use strict";
var blogController_1 = require('../controllers/blogController');
var logger_1 = require('../services/logger');
var AdminRoutes = (function () {
    function AdminRoutes(models) {
        var _this = this;
        this.showBlogs = function (req, res) {
            _this.models.BlogModel.find(function (err, blogs) {
                res.render('admin/blogs', {
                    blogs: blogs
                });
            });
        };
        this.showBlog = function (req, res) {
            _this.models.BlogModel.findById(req.params.id, function (err, blog) {
                res.render('admin/blog', {
                    blog: blog
                });
            });
        };
        this.goCreateBlog = function (req, res) {
            var blog = new _this.models.BlogModel({ title: "", body: "", tags: "", path: "" });
            res.render('admin/blog', {
                blog: blog
            });
        };
        this.editCreateBlog = function (req, res) {
            if (req.body.title === undefined || req.body.tags === undefined || req.body.body === undefined || req.body.published === undefined) {
                logger_1.default.logger.error("Error: No form data for Blog creation");
                throw new Error("No form data for Blog creation");
            }
            if (req.body.id != "" && req.body.id !== undefined) {
                logger_1.default.logger.info("Updating blog " + req.body.path);
                _this.blogController.updateBlog(req.body.title, req.body.tags, req.body.body, req.body.path, req.body.published, req.body.id);
            }
            else {
                logger_1.default.logger.info("Saving blog " + req.body.path);
                _this.blogController.saveBlog(req.body.title, req.body.tags, req.body.body, req.body.path, req.body.published);
            }
            var blog = new _this.models.BlogModel({ title: "", body: "", tags: "", path: "" });
            res.render('admin/blog', {
                blog: blog
            });
        };
        this.deleteBlog = function (req, res) {
            logger_1.default.logger.info("Deleting blog " + req.params.id);
            _this.blogController.deleteBlog(req.params.id);
            res.json({ status: 'success' });
        };
        this.models = models;
        this.blogController = new blogController_1.default(this.models);
    }
    AdminRoutes.prototype.configure = function (app) {
        app.get('/admin/blogs', require('connect-ensure-login').ensureLoggedIn(), this.showBlogs);
        app.post('/admin/blog', require('connect-ensure-login').ensureLoggedIn(), this.editCreateBlog);
        app.get('/admin/blog', require('connect-ensure-login').ensureLoggedIn(), this.goCreateBlog);
        app.get('/admin/blog/delete/:id', require('connect-ensure-login').ensureLoggedIn(), this.deleteBlog);
        app.get('/admin/blog/edit/:id', require('connect-ensure-login').ensureLoggedIn(), this.showBlog);
    };
    ;
    return AdminRoutes;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = AdminRoutes;
//# sourceMappingURL=adminRoutes.js.map