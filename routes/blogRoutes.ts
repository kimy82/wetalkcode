import * as express from "express";
import Models from '../model/modelsBuild';
import BlogController from '../controllers/blogController';
import Logger from '../services/logger';

class BlogRoutes{
  public models:Models;
  public blogController: BlogController;

  constructor(models: Models){
    this.models = models;
    this.blogController = new BlogController(this.models);
  }

  public configure(app: express.Application){
    app.get('/blog/view/:path', this.viewBlog);
    app.get('/blog/comments/:path', this.getBlogComments);
    app.post('/blog/comment/create', this.createBlogComment);
  }

    public viewBlog = (req: express.Request, res: express.Response) => {
      this.blogController.getComments(req.params.path, function(err, comments) {
          res.render('blogs/' + req.params.path + "/" + req.params.path, {
              comments: JSON.stringify(comments).replace(/[\/\\]/g, '_'),
              path: req.params.path
          });

      });
    }

    public getBlogComments = (req: express.Request, res: express.Response) => {
      this.blogController.getComments(req.params.path, function(err, comments) {
          res.json(comments);
      });
    }

    public createBlogComment = (req: express.Request, res: express.Response) => {
      this.blogController.saveComment(req.body.name, req.body.body, req.body.path);
      res.json({status: 'success'});
    }
}

export default BlogRoutes;
