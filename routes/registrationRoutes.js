"use strict";
var userController_1 = require('../controllers/userController');
var encryptService_1 = require('../services/encryptService');
var emailService_1 = require('../services/emailService');
var logger_1 = require('../services/logger');
var settings_1 = require('../configuration/settings');
var RegistrationRoutes = (function () {
    function RegistrationRoutes(models) {
        var _this = this;
        this.register = function (req, res) {
            if (req.body.username === undefined || req.body.company === undefined || req.body.email === undefined || req.body.password === undefined ||
                req.body.username === '' || req.body.company === '' || req.body.email === '' || req.body.password === '') {
                res.render("errorPage", { errorMessage: 'Registration form not complete!' });
                return;
            }
            _this.userController.findByEmail(req.body.email, function (err, users) {
                if (users.length == 0) {
                    logger_1.default.logger.info("User registering " + req.body.email);
                    _this.userController.createUser(req.body.username, req.body.company, req.body.email, req.body.password);
                    _this.emailService.sendEmailVerification(req.body.username, req.body.company, req.body.email, req.body.password, req.headers['host']);
                    res.render("client/index");
                }
                else {
                    logger_1.default.logger.info("User already registered " + req.body.email);
                    if (!settings_1.default.isProduction())
                        _this.userController.deleteUser(users[0]._id);
                    res.render("errorPage", { errorMessage: 'User already exist!' });
                }
            });
        };
        this.verifyUserFromEmail = function (req, res) {
            logger_1.default.logger.info("User verifying account " + req.params.email);
            var hash = req.query.key;
            var email = req.params.email;
            _this.userController.findByEmail(email, function (err, users) {
                var user = users[0];
                if (encryptService_1.default.isValid(user.username + user.company, hash)) {
                    user.enabled = true;
                    user.save();
                    logger_1.default.logger.info("User verified" + user.email);
                    res.redirect("/client/dashboard");
                }
                else {
                    logger_1.default.logger.info("User couldn't be verified. Email: " + user.email);
                    res.render("errorPage", { errorMessage: 'User could not be verified!' });
                }
            });
        };
        this.models = models;
        this.userController = new userController_1.default(models);
        this.emailService = new emailService_1.default();
    }
    RegistrationRoutes.prototype.configure = function (app) {
        app.post('/register', this.register);
        app.get('/verify/:email', this.verifyUserFromEmail);
    };
    return RegistrationRoutes;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = RegistrationRoutes;
//# sourceMappingURL=registrationRoutes.js.map