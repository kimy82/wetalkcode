import * as express from "express";
import Models from '../model/modelsBuild';
import UserController from '../controllers/userController';
import EncryptService from '../services/encryptService';
import Constants from '../services/constants';
import EmailService from '../services/emailService';
import Logger from '../services/logger';
import Settings from '../configuration/settings';


class RegistrationRoutes{
  public models:Models;
  public userController:UserController
  public emailService: EmailService;

  constructor(models: Models){
    this.models = models;
    this.userController = new UserController(models);
    this.emailService = new EmailService();
  }

  public configure(app: express.Application){
    app.post('/register', this.register);
    app.get('/verify/:email', this.verifyUserFromEmail);
  }

  public register = (req: express.Request, res: express.Response) => {
    if(req.body.username === undefined || req.body.company === undefined || req.body.email === undefined || req.body.password === undefined ||
       req.body.username === '' || req.body.company === '' || req.body.email === '' || req.body.password === ''){
      res.render("errorPage", {errorMessage: 'Registration form not complete!'});
      return;
    }
    this.userController.findByEmail(req.body.email, (err:Error, users) => {
        if(users.length == 0 ){
          Logger.logger.info("User registering " + req.body.email);
          this.userController.createUser(req.body.username, req.body.company, req.body.email, req.body.password);
          this.emailService.sendEmailVerification(req.body.username, req.body.company, req.body.email, req.body.password, req.headers['host'] );
          res.render("client/index");
        } else {
          Logger.logger.info("User already registered " + req.body.email);
          if(!Settings.isProduction())
            this.userController.deleteUser(users[0]._id);
          res.render("errorPage", {errorMessage: 'User already exist!'});
        }
    });
  }

  public verifyUserFromEmail = (req: express.Request, res: express.Response) => {
    Logger.logger.info("User verifying account " + req.params.email);
    var hash = req.query.key;
    var email = req.params.email;
    this.userController.findByEmail(email, function(err: Error, users){
        var user = users[0];
        if(EncryptService.isValid(user.username + user.company, hash)){
          user.enabled = true;
          user.save();
          Logger.logger.info("User verified" + user.email);
          res.redirect("/client/dashboard");
        } else {
          Logger.logger.info("User couldn't be verified. Email: " + user.email);
          res.render("errorPage",{errorMessage: 'User could not be verified!'});
        }
    });
  }
}
export default RegistrationRoutes;
