var gulp = require('gulp');
var source = require('vinyl-source-stream'); // Used to stream bundle for further handling
var browserify = require('browserify');
var watchify = require('watchify');
var reactify = require('reactify');
var concat = require('gulp-concat');
var less = require('gulp-less');
var path = require('path');
var LessAutoprefix = require('less-plugin-autoprefix');
var autoprefix = new LessAutoprefix({
    browsers: ['last 9 Chrome versions', 'last 10 Firefox versions','last 10 Safari versions', 'last 10 versions']
});


/**
 * Less compilation gulp task
 **/
gulp.task('less', function() {
    return gulp.src('./assets/static/less/**/*.less')
        .pipe(less({
            plugins: [autoprefix],
            paths: [path.join(__dirname, 'common')]
        }))
		      .pipe(gulp.dest('./assets/static/build/css/'));
});

/**
 * React JS gulp task.
 **/
gulp.task('browserify', function() {

    var browserifyWTK = browserify({
        entries: ['./assets/static/js/react/components.js'], // Only need initial file, browserify finds the deps
        transform: [reactify], // We want to convert JSX to normal javascript
        plugin: [watchify],
        debug: true, // Gives us sourcemapping
        cache: {},
        packageCache: {},
        fullPaths: true // Requirement of watchify
    });

    function handleErrors(error) {
        console.log('******  Start of Error  ******');
        console.log(error.message);
        console.log('******  End of Error  ******');
    }

    console.log("Eii Starting watcher");

    browserifyWTK.on('update', transformationWTK);

    transformationWTK();

    function transformationWTK() { // When any files update
        var updateStart = Date.now();
        console.log('Updating!');
        browserifyWTK.bundle()
            .on('error', handleErrors)
            .pipe(source('components.js'))
            .pipe(gulp.dest('./assets/static/build/'));
        console.log('Updated!', (Date.now() - updateStart) + 'ms');
    }
});
